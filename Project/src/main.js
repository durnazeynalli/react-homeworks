let tabs = document.querySelector(".tabs");
let contents = document.querySelector(".tabs-content");

tabs.addEventListener('click', (e) => {
    const { target } = e;
    let tabsList = [];
    for(let el of tabs.children) {
        el.classList.remove('active');
        tabsList.push(el);
    }
    target.classList.add('active');
    const indexOfActiveTab = tabsList.indexOf(document.querySelector('.active'));
    for(let el of contents.children) {
        el.classList.remove('act')
    }
    contents.children[indexOfActiveTab].classList.add('act')
});





const filtersTitlesContainer = document.getElementById('filter-titles');
const projects = document.querySelectorAll('.projects-item');

filtersTitlesContainer.addEventListener('click', (e) => {
    // console.log(e.target.classList);
    if (e.target.classList.contains('filter-title')) {
        const title = e.target;
        const type = title.dataset.filterby || "projects-item";
        const isActive = title.classList.contains('active');

        if (!isActive) {
            document.querySelector('.filter-title.active').classList.remove('active');
            title.classList.add('active');

            filterByClassName(projects,type);
        }
    }

});


function filterByClassName(elements, className) {
    for (let element of elements) {
        element.hidden = !element.classList.contains(className);
    }
}