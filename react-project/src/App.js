import React from 'react';
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";

import {NoteContextProvider} from "./context/notes";
import {Homepage, Create, OpenNote} from "./pages";
import { Header } from "./commons";

function App() {
  return (
      <NoteContextProvider>
        <Router>
            <Header />
          <Switch>
            <Route exact path ="/" component = {Homepage} />
            <Route path ="/create" component = {Create} />
            <Route path="/opennote/:id" component={OpenNote} />
          </Switch>
        </Router>
      </NoteContextProvider>
  );
}

export default App;
