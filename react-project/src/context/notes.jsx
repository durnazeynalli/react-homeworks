import React, {createContext, useReducer} from "react";

export const NoteContext = createContext();

const ADD_NOTE = 'ADD_NOTE';

const addNoteAC = payload => ({
    type: ADD_NOTE,
    payload
});

const initialState = {
    notes: [
        {
            id: 1,
            title: "hello world",
            text: "Lorem ipsum dolor sit amet.",
            date: 1585583586731,
            color: "#d32727"
        },
        {
            id: 2,
            title: "hello world",
            text: "Lorem ipsum dolor sit amet.",
            date: 1585583586731,
            color: "#3a2c84"
        },
        {
            id: 3,
            title: "hello world",
            text: "Lorem ipsum dolor sit amet.",
            date: 1585583586731,
            color: "#ef8e0b"
        },
        {
            id: 4,
            title: "hello world",
            text: "Lorem ipsum dolor sit amet.",
            date: 1585583586731,
            color: "#516f55"
        }
    ]
};


function reducer(state, { type, payload }) {
    switch (type) {
        case ADD_NOTE:
            return {
                ...state,
                notes: [...state.notes, payload]
            };
        default:
            return state;
    }
}


export const NoteContextProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const addNote = note => dispatch(addNoteAC(note));

    return (
        <NoteContext.Provider value={{...state, addNote}}>{children}</NoteContext.Provider>
    )
};