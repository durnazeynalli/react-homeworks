import React, {useContext} from "react";

import {NoteContext} from "../../context/notes";
import {Container, Row} from "../../commons";
import {Note} from '../../components'

export const Homepage = () => {

    const {notes} = useContext(NoteContext);

    return (
        <Container>
            <h3>Homepage</h3>
            <Row>
                {notes.map(note => (
                    <Note key={note.id} note={note} />
                ))}
            </Row>
        </Container>
    )

};